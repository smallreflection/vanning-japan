// updated simple Gulp 4 taskrunner for LESS

const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp.src('./scss/styles.scss')
        .pipe(sass().on('error', console.error.bind(console)))
        .pipe(gulp.dest('./css'))
});

gulp.task('default', function() {
    gulp.watch('./scss/**/*.scss', gulp.parallel('sass'));
});
